# Copyright 2020 The StackStorm Authors.
# Copyright (C) 2020 Extreme Networks, Inc - All Rights Reserved
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os

from setuptools import setup, find_packages
from dist_utils import check_pip_version
from dist_utils import fetch_requirements
from dist_utils import parse_version_string
# Monkey patch to avoid version normalization like '2.9dev' -> '2.9.dev0', (https://github.com/pypa/setuptools/issues/308)
# NOTE: This doesn't work under Bionic
# from setuptools.extern.packaging import version
# version.Version = version.LegacyVersion

check_pip_version()

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
REQUIREMENTS_FILE = os.path.join(BASE_DIR, 'requirements.txt')
INIT_FILE = os.path.join(BASE_DIR, 'st2authbxq', '__init__.py')

version = parse_version_string(INIT_FILE)
install_reqs, dep_links = fetch_requirements(REQUIREMENTS_FILE)

setup(
    name='st2authbxq',
    version=version,
    description='Customized authentication backend for LDAP.',
    author='StackStorm, Inc.',
    author_email='noreply@nowhere.com',
    url='https://gitlab.com/bxq/st2authbxq',
    license='Apache License (2.0)',
    download_url='https://gitlab.com/bxq/st2authbxq/',
    classifiers=[
        'License :: OSI Approved :: Apache Software License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        'Environment :: Console',
    ],
    platforms=['Any'],
    scripts=[],
    provides=['st2authbxq'],
    packages=find_packages(),
    include_package_data=True,
    install_requires=install_reqs,
    dependency_links=dep_links,
#   test_suite='tests',
    entry_points={
        'st2auth.backends.backend': [
            'bxqldap = st2authbxq.bxqldap_backend:BXQAuthBackendLDAP',
        ],
    },
    zip_safe=False
)
